import numpy as np
import random

class PLA:
    def __init__(self, num_features, x_train, y_train, M):
        self.weights = np.zeros(num_features)
        self.x_train = x_train
        self.y_train = y_train
        self.M = M
        self.update_count = 0

    def predict(self, x):
        product = np.dot(self.weights, x)
        if(not product):
            return 1
        else:
            return np.sign(product)
    
    def update(self, x, y):
        self.weights += x * y
        self.update_count += 1
        print(f"{self.weights[0]}")

        
    
    def Ein(self):
        err_count = 0
        for i in range(len(x_train)):
            err_count += self.predict(x_train[i]) != y_train[i]

        return err_count / len(x_train)
    
    def train(self):
        count = 0
        train_len = len(x_train)
        
        while count != self.M:
            i = random.choice(range(train_len))

            x_i = self.x_train[i]
            y_i = self.y_train[i]

            if self.predict(x_i) == y_i:
                count += 1
            else:
                self.update(x_i,y_i)
                count = 0


        
TOTAL_RUNS = 1000

if __name__ == "__main__":
    w0x0s = np.array([])
    for i in range(TOTAL_RUNS):
        x_train = []
        y_train = []
        with open("hw1_train.dat", "r") as data:
            for line in data:
                line = line.strip()
                values = line.split()
                x_train.append(np.array([0.1126]+values[0:10], dtype=np.float64))
                y_train.append(np.float64(values[10]))

        pla = PLA(len(x_train[0]), x_train, y_train, len(x_train)*4)

        pla.train()

        w0x0s = np.append(w0x0s, pla.weights[0]*0.1126)
    
    print(f"The median is: {np.median(w0x0s)}")
    #print(f"{w0x0s})")
